#+TITLE: Introduction to FOSS, the Debian way
#+AUTHOR: Abraham Raji
#+EMAIL: abraham@debian.org
#+DATE: March 18, 2023
#+DESCRIPTION: FOSS as of 2020
#+KEYWORDS: foss
#+LANGUAGE:  en
#+OPTIONS:  num:nil ^:{} toc:nil
#+LaTeX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: Luebeck
#+BEAMER_HEADER: \subtitle{i.e. the right way}}
#+EXCLUDE_TAGS: noexport
#+PROPERTY:  header-args :eval no
#+REVEAL_THEME: league
#+REVEAL_DEFAULT_SLIDE_BACKGROUND: #1a1626
#+REVEAL_TITLE_SLIDE_BACKGROUND: #1a1626

* Introduction to FOSS
*the Debian way*








                                                         നമസ്കാരം
                                                       வணக்கம்
                                                           नमस्ते
                                                            안녕
                                                       こんにちは
* Ground Rules
- No doubts are stupid. People who think otherwise are stupid.
- Regardless of whether you agree with me or disagree with me it is important you understand me.
* Introduction
- My name is [[https://abrahamraji.in][Abraham Raji]]
- I'm known online as [[mailto:avronr@tuta.io][avronr]].
- I write code for a living.
- Web. Cloud. Programming Languages. Systems Engineering.
* But does FOSS make sense?
#+begin_quote
If the users don't control the program, the program controls the users. With proprietary software, there is always some entity, the "owner" of the program, that controls the program—and through it, exercises power over its users. A nonfree program is a yoke, an instrument of unjust power.

--- Richard Mathew Stallman [[https://www.gnu.org/philosophy/free-software-even-more-important.html][Free Software Is Even More Important Now (September 2013)]]
#+end_quote
* Free Software Definition
Free and Open source software is any piece of software that gives it's users the four essential freedoms.
- The freedom to run the program as you wish, for any purpose (freedom 0).
- The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1).
- Access to the source code is a precondition for this. The freedom to redistribute copies so you can help others (freedom 2).
- The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.
* Free Software succeeded because
- Users would rather have software that they were able to fix themselves
- Users would rather use software that they can trust
- Developers now received suggestions and help from skilled users so their work was easier
- With more people looking into the software bugs were found and resolved faster.
* Open Source vs Free Software
[[file:./ep000.jpg]]
* Open Source vs Free Software
- A difference in philosophy.
* Can a Company make money out of Free Software?
- Short Answer: Yes
- Real life examples: Redhat, Suse, Canonical, Gitlab and More

[[file:./comp.png]]
* Can an individual make a living out of Free Software?
- Short Answer: Yes.
- You can work at the above mentioned companies.
- You can provide support for free software.
- You can work on or created projects supported by a community or corporate.

* Free Software in India
- Free Software Community of India (FSCI) =fsci.in=
- Swathanthra Malayalam Computing =smc.org.in=
- Mozilla Kerala =mozillakerala.com=
- Leapcode =leapcode.io=
- Various Free Software and Linux User Groups
* Questions
- Ask Away !
* Get in touch with me
- Mail: abraham@debian.org
- IRC: abrahamr
- Matrix: abraham_raji@matrix.org
